﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Net;
using VkTools.Model;
using VkTools.Example.Classes;
using VkTools.Enums;
using System.IO;

namespace VkTools.Example
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private string _downloadPath = @"C:\Users\Paul\Music\";
        private string _downloadPath = Directory.GetCurrentDirectory();
        private System.Timers.Timer _timer;
        private int _maxDownloadThreads = 10;
        private ObservableCollection<AudioView> _audioRecords = new ObservableCollection<AudioView>();
        public ObservableCollection<AudioView> AudioRecords { get { return _audioRecords; } }

        public Queue<AudioView> _downloadQueue = new Queue<AudioView>();

        public VkConnector Vk = VkConnector.Instance;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }


        private void Authorize_Click(object sender, RoutedEventArgs e)
        {
            Vk.AppId = "3499074";
            Vk.Settings = "offline,audio";
            Vk.Authorize();
            Vk.Authorized += (token, userId) =>
            {
                AuthorizeButton.Background = Brushes.LightGreen;
                LoadAudioListButton.IsEnabled = true;

                long[] uids = new long[] { userId, 1 };
                ProfileField[] fields = new ProfileField[] { ProfileField.All };

                UserNameLabel.Content = "Logged in as " + Vk.User.UsersGet(uids, fields).FirstOrDefault().FullName;
            };
        }

        private void LoadAudioListButton_Click(object sender, RoutedEventArgs e)
        {           
            var param = new Dictionary<object, object>();
            param.Add("uid", Vk.UserId);
            _audioRecords.Clear();

            int id = 0;
            Vk.Audio.AudioGet(param).ForEach(x =>
            {
                bool exists = File.Exists(_downloadPath +  x.GetFullName());
                _audioRecords.Add(new AudioView() { Id = ++id, Title = x.Title, Artist = x.Artist, Url = x.Url, Duration = x.Duration, Progress = exists ? 100 : 0, State = exists ? State.Completed : State.Pending, CheckBoxValue = !exists });
            });
            DownloadButton.IsEnabled = true;
        }

        bool _isDownloadInProgress = false;

        private void DownloadButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isDownloadInProgress)
            {
                return;
            }

            _isDownloadInProgress = true;

            MainProgressBar.Maximum = (double)_audioRecords.Sum(x => x.Duration * 100);

            _timer = new System.Timers.Timer();
            _timer.Interval = 1000;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(UpdateProgress);
            _timer.Enabled = true;


            for (int i = 0; i < _maxDownloadThreads; i++)
                (new Thread(DownloadWorker)).Start();
        }

        private void UpdateProgress(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    AudioList.Items.Refresh();

                    MainProgressBar.Value = (double)_audioRecords.Sum(x => x.Duration * x.Progress);

                }));

        }

        private long GetFileSize(Uri uri)
        {
            WebRequest req = System.Net.HttpWebRequest.Create(uri);
            req.Method = "HEAD";
            WebResponse resp = req.GetResponse();

            return int.Parse(resp.Headers.Get("Content-Length"));
        }

        private void DownloadWorker()
        {
            while (true)
            {
                AudioView audio;
                bool needDownload = true;
                lock (_audioRecords)
                {
                    var downloadingCount = _audioRecords.Count(x => x.State == State.Downloading);
                    audio = _audioRecords.FirstOrDefault(x => x.State == State.Pending && x.CheckBoxValue == true);

                    if (audio != null)
                    {
                        if (downloadingCount < _maxDownloadThreads)
                        {
                            needDownload = true;
                            audio.State = State.Downloading;
                        }
                        else
                        {
                            needDownload = false;
                        }

                    }
                    else
                    {
                        break;
                    }

                }
                if (!needDownload)
                {
                    Thread.Sleep(250);
                }
                else
                {
                    try
                    {

                        using (WebClient client = new WebClient())
                        {
                            var fullPath = _downloadPath + audio.FileName;
                            if (File.Exists(fullPath) && new FileInfo(fullPath).Length == GetFileSize(new Uri(audio.Url)))
                            {
                                Thread.Sleep(10);
                                audio.Progress = (double)100;
                                audio.State = State.Completed;
                            }
                            else
                            {
                                client.DownloadProgressChanged += (s, a) =>
                                {
                                    audio.Progress = a.ProgressPercentage;
                                };
                                client.DownloadFileCompleted += (s, a) => 
                                    {
                                        audio.State = State.Completed;
                                        audio.CheckBoxValue = false;
                                    };
                                client.DownloadFileAsync(new Uri(audio.Url), fullPath);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        audio.State = State.Error;
                    }
                }
            }
        }


        private void CheckAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var audio in _audioRecords)
            {
                audio.CheckBoxValue = true;
            }
            AudioList.Items.Refresh();
        }

        private void UnheckAllButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var audio in _audioRecords)
            {
                audio.CheckBoxValue = false;
            }
            AudioList.Items.Refresh();
        }

        private void ReverseSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var audio in _audioRecords)
            {
                audio.CheckBoxValue = !audio.CheckBoxValue;
            }
            AudioList.Items.Refresh();
        }

    }
}
