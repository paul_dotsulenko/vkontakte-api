﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Example
{
    public enum State
    {
        Pending,
        Downloading,
        Completed,
        Error
    }
}
