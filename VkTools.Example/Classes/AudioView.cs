﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkTools.Model;
using VkTools.Example.Classes;
using System.IO;

namespace VkTools.Example
{
    public class AudioView : Audio
    {
        public int Id { get; set; }
        public State State { get; set; }
        public double Progress { get; set; }
        public string FileName
        {
            get
            {
                return  this.GetFullName();
            }
        }
        public bool CheckBoxValue { get; set; }
    }
}
