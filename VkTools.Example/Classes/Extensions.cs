﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkTools.Model;

namespace VkTools.Example.Classes
{
    public static class Extensions
    {
        public static string CleanFileName(this string file)
        {
            file = string.Concat(file.Split(System.IO.Path.GetInvalidFileNameChars(), StringSplitOptions.RemoveEmptyEntries));

            if (file.Length > 250)
            {
                file = file.Substring(0, 250);
            }
            return file;
        }
        public static string GetFullName(this Audio audio)
        {
            return string.Format(@"{0} - {1}.mp3", audio.Artist, audio.Title).CleanFileName();
        }
    }
}
