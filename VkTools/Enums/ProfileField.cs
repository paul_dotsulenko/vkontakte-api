﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Enums
{
    public sealed class ProfileField
    {
        private readonly string _name;

        public static readonly ProfileField None = new ProfileField("");

        public static readonly ProfileField Uid = new ProfileField("uid");
        public static readonly ProfileField FirstName = new ProfileField("first_name");
        public static readonly ProfileField LastName = new ProfileField("last_name");
        public static readonly ProfileField NickName = new ProfileField("nickname");
        public static readonly ProfileField ScreenName = new ProfileField("screen_name");
        public static readonly ProfileField Sex = new ProfileField("sex");
        public static readonly ProfileField BirthDate = new ProfileField("bdate");
        public static readonly ProfileField City = new ProfileField("city");
        public static readonly ProfileField Country = new ProfileField("country");
        public static readonly ProfileField Timezone = new ProfileField("timezone");
        public static readonly ProfileField Photo = new ProfileField("photo");
        public static readonly ProfileField PhotoMedium = new ProfileField("photo_medium");
        public static readonly ProfileField PhotoBig = new ProfileField("photo_big");
        public static readonly ProfileField HasMobile = new ProfileField("has_mobile");
        public static readonly ProfileField Rate = new ProfileField("rate");
        public static readonly ProfileField Contacts = new ProfileField("contacts");
        public static readonly ProfileField Education = new ProfileField("education");
        public static readonly ProfileField Counters = new ProfileField("counters");
        public static readonly ProfileField Online = new ProfileField("online");

        public static readonly ProfileField All = new ProfileField("uid,first_name,last_name,nickname,screen_name,sex,bdate,city,country,timezone,photo,photo_medium,photo_big,has_mobile,rate,contacts,education,online,counters");

        private ProfileField(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
