﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Enums
{
    public sealed class ResponseType
    {
        private readonly string _name;

        public static readonly ResponseType Token = new ResponseType("token");
        public static readonly ResponseType Code = new ResponseType("code");

        private ResponseType(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
