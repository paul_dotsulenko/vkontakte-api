﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Enums
{
    public sealed class DisplayType
    {
        private readonly string _name;

        public static readonly DisplayType Page = new DisplayType("page");
        public static readonly DisplayType Popup = new DisplayType("popup");
        public static readonly DisplayType Mobile = new DisplayType("mobile");

        private DisplayType(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
