﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Net;
using VkTools.Enums;
using VkTools.Model;
using System.Xml;
using VkTools.Classes;
using System.Xml.Linq;

namespace VkTools
{
    public class VkConnector
    {
        public string AppId { get; set; }
        public string Settings { get; set; }
        public string RedirectUri { get; set; }
        public DisplayType DisplayType { get; set; }
        public ResponseType ResponseType { get; set; }

        public string AuthToken { get; set; }
        public long UserId { get; private set; }

        public bool IsAuthorized { get; private set; }

        public AudioMethods Audio { get; private set; }
        public UserMethods User { get; private set; }

        public event Action<string, long> Authorized;

        private const string methodUri = "https://api.vk.com/method/";

        public VkConnector()
        {
            DisplayType = DisplayType.Popup;
            ResponseType = ResponseType.Token;
            RedirectUri = "https://oauth.vk.com/blank.html";
            IsAuthorized = false;
            Audio = new AudioMethods(this);
            User = new UserMethods(this);

        }

        public void Authorize()
        {
            var authWindow = new AuthWindows();
            //authWindow.Show();
            authWindow.webBrowser.Navigate(CreateAuthUrl(AppId, Settings, RedirectUri, DisplayType, ResponseType));
            authWindow.webBrowser.Navigated += (sender, e) => CatchToken(sender, e, authWindow);
            Authorized += (token, userId) =>
                {
                    AuthToken = token;
                    UserId = userId;
                    IsAuthorized = true;
                    authWindow.webBrowser.Navigated -= (sender, e) => CatchToken(sender, e, authWindow);
                    authWindow.Close();
                };
        }

        public XDocument ApiRequest(string method, IDictionary<object, object> parameters)
        {
            if (!IsAuthorized)
            {
                return null;
            }

                string address = methodUri + method + ".xml?" + EncodeParameters(parameters) + "access_token=" + AuthToken;
                var doc = XDocument.Load(address);

            return doc;
        }

        private void CatchToken(object sender, System.Windows.Navigation.NavigationEventArgs e, AuthWindows authWindow)
        {
            var uri = e.Uri.Fragment.Replace("#", string.Empty).Split('&');
            string token = ""; 
            long userId = 0;
            foreach (var item in uri)
            {
                if (item.StartsWith("access_token"))
                {
                    token = item.Split('=')[1];
                }
                else if (item.StartsWith("user_id"))
                {
                    userId = long.Parse(item.Split('=')[1]);
                }
            }
            if (token.Any() && userId != 0)
            {
                Authorized(token, userId);
            }
            else
            {
                authWindow.Show();
            }
        }


        private string EncodeParameters(IDictionary<object, object> parameters)
        {
            var sb = new StringBuilder();

            foreach (var parameter in parameters)
            {
                sb.AppendFormat("{0}={1}&", parameter.Key, parameter.Value);
            }

            return sb.ToString();
        }

        private string CreateAuthUrl(string appId, string settings, string redireсtUrl, DisplayType display, ResponseType responseType)
        {
            var sb = new StringBuilder("https://oauth.vk.com/authorize?");
            sb.AppendFormat("client_id={0}&", appId);
            sb.AppendFormat("scope={0}&", settings);
            sb.AppendFormat("redirect_uri={0}&", redireсtUrl);


            sb.AppendFormat("display={0}&", display);
            sb.AppendFormat("response_type={0}", responseType);

            return sb.ToString();
        }
    }
}
