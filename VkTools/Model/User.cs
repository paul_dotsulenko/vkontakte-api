﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Model
{
    public class User
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }
        public string Nickname { get; set; }
        public string ScreenName { get; set; }
        public int? Sex { get; set; }
        public string BirthDate { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public double? Timezone { get; set; }
        public string Photo { get; set; }
        public string PhotoMedium { get; set; }
        public string PhotoBig { get; set; }
        public int? HasMobile { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string Rate { get; set; }
        public string Contacts { get; set; }
        public string Education { get; set; }
        public int? Online { get; set; }
        public string Counters { get; set; }
        public string NameGen { get; set; }
        public long? InvitedBy { get; set; }
    }
}
