﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkTools.Model
{
    public class Audio
    {
        public long Aid { get; set; }
        public long OwnerId { get; set; }
        public string Artist { get; set; }
        public string Title { get; set; }
        public int Duration { get; set; }
        public string Url { get; set; }
        public long? LyricsId { get; set; }
        public long? AlbumId { get; set; }
        public string Performer { get; set; }
    }
}
