﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using VkTools.Model;
namespace VkTools.Classes
{
    public class AudioMethods
    {
        private readonly VkConnector _vkConnector;

        public AudioMethods(VkConnector vkConnector)
        {
            _vkConnector = vkConnector;
        }


        public List<Audio> AudioGet(IDictionary<object, object> parameters)
        {
            var audioList = new List<Audio>();

            var result = _vkConnector.ApiRequest("audio.get", parameters).Descendants("audio");

            foreach (var item in result)
            {
                var audio = new Audio();
                audio.Aid = long.Parse(item.Element("aid").Value);
                audio.OwnerId = long.Parse(item.Element("owner_id").Value.ToString());
                audio.Artist = item.Element("artist").Value.ToString();
                audio.Title = item.Element("title").Value.ToString();
                audio.Duration = int.Parse(item.Element("duration").Value.ToString());
                audio.Url = item.Element("url").Value.ToString();

                audioList.Add(audio);
            }

            return audioList;
        }
    }
}
