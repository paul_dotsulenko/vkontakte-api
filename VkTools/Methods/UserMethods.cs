﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using VkTools.Enums;
using VkTools.Model;
namespace VkTools.Classes
{
    public class UserMethods
    {
        private readonly VkConnector _vkConnector;

        public UserMethods(VkConnector vkConnector)
        {
            _vkConnector = vkConnector;
        }
        public IEnumerable<User> UsersGet(IEnumerable<long> uids, IEnumerable<ProfileField> fields)
        {
            var userList = new List<User>();
            var parameters = new Dictionary<object, object>();
            var selectedFields = new StringBuilder();
            if (fields.Contains(ProfileField.All))
            {
                selectedFields.Append(ProfileField.All);
            }
            else if (fields.Contains(ProfileField.None))
            {
                 selectedFields.Append(ProfileField.None);
            }
            else
            {
                foreach (var field in fields)
                {
                    selectedFields.Append(field);
                }
            }

            parameters.Add("uids", string.Join(",", uids.ToArray()));
            parameters.Add("fields", selectedFields.ToString());

            var result = _vkConnector.ApiRequest("users.get", parameters).Descendants("user");

            foreach (var item in result)
            {
                var user = new User();
                user.Id = long.Parse(item.Element(ProfileField.Uid.ToString()).Value);
                user.FirstName = item.Element(ProfileField.FirstName.ToString()).Value;
                user.LastName = item.Element(ProfileField.LastName.ToString()).Value;

                var nickName = item.Element("nickname");
                if (nickName != null)
                {
                    user.Nickname = nickName.Value;
                }
                
                user.ScreenName = item.Element(ProfileField.ScreenName.ToString()).Value;

                var sex = item.Element(ProfileField.Sex.ToString());
                if (sex != null)
                {
                    user.Sex = int.Parse(sex.Value);
                }

                var birthDate = item.Element(ProfileField.BirthDate.ToString());
                if (birthDate != null)
                {
                    user.BirthDate = birthDate.Value;
                }

                var city = item.Element(ProfileField.City.ToString());
                if (city != null)
                {
                    user.City = city.Value;
                }

                user.City = item.Element(ProfileField.City.ToString()).Value;
                user.Country = item.Element(ProfileField.Country.ToString()).Value;
                user.Timezone = double.Parse(item.Element(ProfileField.Timezone.ToString()).Value);
                user.Photo = item.Element(ProfileField.Photo.ToString()).Value;
                user.PhotoMedium = item.Element(ProfileField.PhotoMedium.ToString()).Value;
                user.PhotoBig = item.Element(ProfileField.PhotoBig.ToString()).Value;
                user.HasMobile = int.Parse(item.Element(ProfileField.HasMobile.ToString()).Value);


                var homePhone = item.Element("home_phone");
                if (homePhone != null)
                {
                    user.HomePhone = homePhone.Value;
                }


                var rate = item.Element(ProfileField.Rate.ToString());
                if (rate != null)
                {
                    user.Rate = rate.Value;
                }

                var education = item.Element(ProfileField.Education.ToString());
                if (education != null)
                {
                    user.Education = education.Value;
                }

                user.Online = int.Parse(item.Element(ProfileField.Online.ToString()).Value.ToString());

                var counters = item.Element(ProfileField.Counters.ToString());
                if (counters != null)
                {
                    user.Counters = counters.Value;
                }

                var namegen = item.Element("name_gen");
                if (namegen != null)
                {
                    user.NameGen = namegen.Value.ToString();
                }

                var invitedBy = item.Element("invited_by");
                if (invitedBy != null)
                {
                    user.InvitedBy = long.Parse(invitedBy.Value);
                }

                
                userList.Add(user);
            }
      
            return userList;
        }
    }
}
